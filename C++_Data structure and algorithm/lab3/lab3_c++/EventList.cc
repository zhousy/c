//
// EVENTLIST.CC
// Skeleton code for your EventList collection type.
//

#include <iostream>
#include <limits>
#include <cstdlib>
#include <iomanip>   // for dump() function

#include "EventList.h"

using namespace std;

////////////////////////////////////////////////////////////////////
// Here's a suitable geometric random number generator for choosing
// pillar heights.  We treat the middle bit of each random number
// generated as a random coin flip.
////////////////////////////////////////////////////////////////////

int EventList::randomHeight()
{
  int v = 1;
  while ((rand() >> 16) & 0x01) { v++; }
  return v;
}

////////////////////////////////////////////////////////////////////
// Methods for the Lab
////////////////////////////////////////////////////////////////////

//
//Dynamic Control heights
//
void EventList::doubling()
{
	int newHeight=2*head->height;
	EventPillar *head2, *tail2;
	head2 = new EventPillar(headE, newHeight);
	tail2 = new EventPillar(tailE, newHeight);
	EventPillar *p;
	p = head;
	for(int j=0;j<newHeight;j++){
		tail2->next[j] = nullptr;
	}
	for(int j=(head->height-1);j>=0;j--){
		while (p->next[j] != tail){
			p = p->next[j];
		}
		p->next[j] = tail2;
	}
	for (int j=0;j<newHeight;j++){
		if (j<head->height) {
			head2->next[j] = head->next[j];
		}else{
			head2->next[j] = tail2;
		}
	}
	head = head2;
	tail = tail2;
}

//
// EventPillar constructor
//
EventPillar::EventPillar(Event *event, int height)
{
	this->event = event;
	this->height = height;
	next = new EventPillar* [this->height];
}

//
// EventPillar destructor
//
EventPillar::~EventPillar()
{
	delete[] next;
	event=new Event(0,"Deleted");
	height=0;
}

//
// EventList constructor
//
EventList::EventList()
{
	maxheight = 1;
	headE = new Event(-99999,"Head");
	head = new EventPillar(headE, 1);
	tailE = new Event(99999, "Tail");
	tail = new EventPillar(tailE, 1);
	head->next[0] = tail;
	tail->next[0] = nullptr;
}


//
// EventList destructor
//
EventList::~EventList()
{
	delete head;
	delete headE;
	delete tail;
	delete tailE;
}

//
// Add an Event to the list.
//
void EventList::insert(Event *e)
{
	int h=randomHeight();
	EventPillar* newEvent = new EventPillar(e, h);
	if (h > maxheight){
		maxheight = h;
		while (head->height < maxheight){
			doubling();
		}
	}

	EventPillar *p;
	EventPillar *pp;
	pp=head;
	for (int i=maxheight-1;i>=0;i--){
		p=pp;
		while(p->event->year <= e->year){
			pp=p;
			p=p->next[i];
		}
		if (i < newEvent->height){
			pp->next[i]=newEvent;
			newEvent->next[i]=p;
		}
	}
}


//
// Remove all Events in the list with the specified year.
//
void EventList::remove(int year)
{
	EventPillar *p, *pp;
	pp = head;
	for (int i=maxheight;i>=0;i--){
		p=pp;
		while (p->event->year<year){
			pp=p;
			p=p->next[i];
		}
		if(p->event->year==year){
			while(p->event->year==year){
				p=p->next[i];
			}
			pp->next[i]=p;
		}
	}
}


//
// Find all events with greatest year <= input year.
//
vector<Event *> *EventList::findMostRecent(int year)
{
	if (year < head->next[0]->event->year){
		return nullptr;
	}
	int flag;
	EventPillar *p, *pp;
	vector<Event*>* res = new vector<Event*>;
	pp=head;
	for (int i=maxheight;i>=0;i--){
		p=pp;
		while (p->event->year<=year){
			pp=p;
			p=p->next[i];
		}
	}
	if (pp->event->year==year){
		flag=year;
	}else{
		flag=pp->event->year;
	}
	pp=head;
	for (int i=maxheight-1;i>=0;i--){
		p=pp;
		while(p->event->year<flag){
			pp=p;
			p=p->next[i];
		}
	}
	pp=pp->next[0];
	while(pp->event->year==flag){
		res->push_back(pp->event);
		pp=pp->next[0];
	}
	return res;
}

//
// Find all Events within the specific range of years (inclusive).
//
vector<Event *> *EventList::findRange(int first, int last)
{
	EventPillar *p,*pp;
	pp = head;
	for(int i=maxheight-1;i>=0;i--){
		p=pp;
		while (p->event->year<first){
			pp=p;
			p=p->next[i];
		}
	}
	if (p->event->year > last){
		return nullptr;
	}else{
		vector<Event*>* res = new vector<Event*>;
		while(p->event->year <= last){
			res->push_back(p->event);
			p=p->next[0];
		}
		return res;
	}
}


//
// Utility function to print list
// Usage (in Lab3.cc): list->dump(cout);
//
void EventList::dump(ostream& os)
{

  // Your code may look different depending on how you implement
  //   the EventPillar object.  Use below code as an example,
  //   but feel free to modify it to suit your needs.

  os << "Event list: maxUsedHeight: " << maxheight << endl;
  EventPillar *p = head;
  while(p)
  {
    int key = (p->event) ? p->event->year : numeric_limits<int>::max();
    os << "Pillar " << p << " key "
      << key << ": ";
    for(int i=0; i < p->height; ++i)
      os << setw(10) << p->next[i];
    os << endl;
    p = p->next[0];
  }
  cout<<endl;

  return;

}
