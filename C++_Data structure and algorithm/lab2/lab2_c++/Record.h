#ifndef __RECORD_H
#define __RECORD_H

#include <vector>

class Record {
public:
  const char *key;
  std::vector<int> positions;
  
  Record(const char *s)
    : key{s}
  {}

  char* getKey(int l){
	  char* k=new char[l];
	  for (int i=0;i<l;i++){
		  k[i]=key[i];
	  }
	  return k;
  }

};

#endif
