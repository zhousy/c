// STRINGTABLE.CC
// A hash table mapping strings to their positions in the the pattern sequence
// You get to fill in the methods for this part.  Since we're passing around
// raw character strings, I've made the match length a parameter of the
// hash table so we don't have to keep figuring it out on each call.

#include <iostream>
#include <cmath>
#include <cstring>

#include "StringTable.h"

using namespace std;

/////////////////////////////
// HINT: the built-in C++ function strncmp 
// 	 (from the cstring library included above) may be used to 
// 	 compare two C-style strings of type (const char*) for equality.
// 	 format: strncmp(const char* s1, const char* s2, length) compares
// 	 up to length characters of s1 and s2, returning an int equal to
// 	 0 if they are identical and nonzero otherwise.
/////////////////////////////

//
// Create an empty table big enough to hold maxSize records.
//
StringTable::StringTable(int maxSize, int imatchLength)
  : matchLength{imatchLength}
{

	int size=1;
	while (size<(4*maxSize)){
		size=size*2;
	}
	maxLength=size;
	length=0;
	myList=new Record *[maxLength];
	myListKey=new int [maxLength];
	char* s="d";
	d=new Record(s);
	for (int i=0;i<maxLength;i++){
		myListKey[i]=-1;
	}
}

//
//constructor without maxSize parameter
//
StringTable::StringTable(int imatchLength)
  : matchLength{imatchLength}
{
	char* s="d";
	d=new Record(s);
	maxLength=2;
	length=0;
	myList=new Record *[maxLength];
	myListKey=new int [maxLength];
	for (int i=0;i<maxLength;i++){
		myListKey[i]=-1;
	}
}

//
//assistant debug function
//
void StringTable::showit(void){
	cout<<maxLength<<" "<<length<<endl;
	for (int i=0;i<maxLength;i++){
		if (myListKey[i]!=-1) {
			cout<<myList[i]->key<<endl;
		}
	}
}

//
//Insert function to prevent memory issue in
//recursive call insert function it self
//
bool StringTable::doubleInsert(Record *r){

	int hashKey=toHashKey(r->key);
	int baseH=baseHash(hashKey);
	int stepH=stepHash(hashKey);

	int hash=baseH;
	while (true) {
		if ((myListKey[hash]<0)||(myList[hash]==d)) {
			length++;
			myList[hash]=r;
			myListKey[hash]=hashKey;
			return true;
		}
		hash+=stepH;
		hash=hash%maxLength;
		if (hash==baseH){
			return false;
		}
	}
	return false;
}

//
//when table not big enough then doubling
//
void StringTable::doubling(void)
{
	int mLength=maxLength;
	maxLength=maxLength*2;
	Record** myList2=myList;
	int* myListKey2=myListKey;
	length=0;
	myList=new Record *[maxLength];
	myListKey=new int [maxLength];
	for (int i=0;i<maxLength;i++){
		myListKey[i]=-1;
	}
	for (int i=0;i<mLength;i++){
		if (myListKey2[i] >=0){
			bool temp=doubleInsert(myList2[i]);
		}
	}
}

//
//my own function about how to compare 2 char[]
//
bool StringTable::scmp(const char *s,const char *s2){
	for (int i=0;i<matchLength;i++){
		if (s[i]!=s2[i]) {
			return false;
		}
	}
	return true;
}

//
// Insert a Record r into the table.  Return true if
// successful, false if the table is full.  You shouldn't ever
// get two insertions with the same key value, but you may
// simply return false if this happens.
//

bool StringTable::insert(Record *r)
{
	int hashKey=toHashKey(r->key);
	int baseH=baseHash(hashKey);
	int stepH=stepHash(hashKey);
	int hash=baseH;
	while (true) {
		if ((myListKey[hash]>0)&&(hashKey==myListKey[hash])&&(scmp(r->key,myList[hash]->key))){
			return false;
		}
		if ((myListKey[hash]<0)||(myList[hash]==d)) {
			length++;
			myList[hash]=r;
			myListKey[hash]=hashKey;
			if ((4*length)>=(maxLength)) {
				doubling();
			}
			return true;
		}
		hash+=stepH;
		hash=hash % maxLength;
		if (hash==baseH){
			return false;
		}
	}
  return false;
}


//
// Delete a Record r from the table.  Note that you'll have to
// find the record first unless you keep some extra info in
// the Record structure.
//
void StringTable::remove(Record *r)
{
	int hashKey=toHashKey(r->key);
	int baseH=baseHash(hashKey);
	int stepH=stepHash(hashKey);
	int hash=baseH;
	while ((myListKey[hash]!=-1)) {
		if ((hashKey==myListKey[hash])&&(scmp(r->key,myList[hash]->key))){
			myList[hash]=d;
			myListKey[hash]=-2;
			length--;
			return;
		}
		hash+=stepH;
		hash=hash%maxLength;
		if (hash==baseH){
			return ;
		}

	}
	return ;
}


//
// Find a record with a key matching the input.  Return the
// record if it exists, or nullptr if no matching record is found.
//
Record *StringTable::find(const char *key)
{
	int hashKey=toHashKey(key);
	int baseH=baseHash(hashKey);
	int stepH=stepHash(hashKey);
	int hash=baseH;
	while (myListKey[hash]!=-1) {
		if ((hashKey==myListKey[hash])&&(scmp(key,myList[hash]->key))){
			return myList[hash];
		}
		hash+=stepH;
		hash=hash%maxLength;
		if (hash==baseH){
			return nullptr;
		}
	}
	return nullptr;
}


//////////////////////////////////////////////////////////

// Convert a string key into an integer that serves as input to hash
// functions.  This mapping is based on the idea of a linear-congruential
// pesudorandom number generator, in which successive values r_i are 
// generated by computing
//    r_i = ( A * r_(i-1) + B ) mod M
// A is a large prime number, while B is a small increment thrown in
// so that we don't just compute successive powers of A mod M.
//,
// We modify the above generator by perturbing each r_i, adding in
// the ith character of the string and its offset, to alter the
// pseudorandom sequence.
//
// NB: This fcn maps from space of arbitrary strings to ints, so
//     collisions are very possible and NOT resolved, i.e. two
//     distinct input strings may return the same value
//
int StringTable::toHashKey(const char *s) const
{
  int A = 1952786893;
  int B = 367257;
  int v = B;
  
  for (int j = 0; j < matchLength; j++)
    v = A * (v + int(s[j]) + j) + B;
  
  if (v < 0) v = -v;
  return v;

}


int StringTable::baseHash(int hashKey) const
{
  // Fill in your own hash function here
	return int(((hashKey*KNUTHVAL)-int(hashKey*KNUTHVAL))*maxLength);
}


int StringTable::stepHash(int hashKey) const
{
  // Fill in your own hash function here
	int temp = int(((hashKey*PI/5)-int(hashKey*PI/5))*maxLength);
	if ((temp % 2) ==0) {
		temp=temp+1;
	}
	return (temp % maxLength);
}


