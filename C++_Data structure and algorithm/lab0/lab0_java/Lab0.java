//
// LAB0.JAVA
// Main driver code for CSE 241 Lab #0.
//
// WARNING: ANY CHANGES YOU MAKE TO THIS FILE WILL BE DISCARDED
// BY THE AUTO-GRADER!  Make sure your code works with the
// unmodified original driver before you turn it in.
//

public class Lab0 {
    
    public static void main(String args[])
    {
	// read largest number to print as argument
	int limit = Integer.parseInt(args[0]);

	MyThing thing = new MyThing();
	
	thing.count(limit);
    }
}
