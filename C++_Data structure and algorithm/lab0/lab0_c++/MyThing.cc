#include <iostream>

#include "MyThing.h"

using namespace std;

void MyThing::count(int x) 
{
  // Print every odd number between 1 and x (inclusive) in the form
  // 1...
  // 3...
  // <etc>
  // x!
	for(int i=1;i<=x;i++){
		if (((i%2)!=0)&&((i+2)<=x)) {
			cout<<i<<"..."<<endl;
		}
		if (((i%2)!=0)&&((i+2)>x)) {
			cout<<i<<"!"<<endl;
		}
	}
}
