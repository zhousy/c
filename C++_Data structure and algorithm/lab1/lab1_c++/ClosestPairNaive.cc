//
// CLOSEST-PAIR-NAIVE.CC
// Naive implementation of the closest-pair algorithm

#include <iostream>
#include <limits>

#include "ClosestPairNaive.h"

using namespace std;

// bigger than any possible interpoint distance
const double INFTY = numeric_limits<double>::max(); 

//
// findClosestPair()
//
// Given a collection of nPoints points, find and ***print***
//  * the closest pair of points
//  * the distance between them
// in the form "(x1, y1) (x2, y2) distance"
//

// INPUTS:
//  - points (not necessarily sorted)
//  - # of input points
//
void findClosestPairNaive(Point *points[], int nPoints)
{
	//initial
	double sDist=INFTY;
	int fIndex=0,sIndex=0;
	
	//exhaustive algorithm
	for(int i=0;i<nPoints;i++){
		for(int j=i+1;j<nPoints;j++){
			double dist=points[i]->distance(points[j]);
			if (dist<sDist) {
				sDist=dist;
				fIndex=i;
				sIndex=j;
			}
		}
	}
	//format output
	if(points[sIndex]->isLeftOf(points[fIndex])){
		int temp=fIndex;
		fIndex=sIndex;
		sIndex=temp;
	}
	cout<<points[fIndex]<<' '<<points[sIndex]<<' '<<sDist<<endl;

}
