//
// CLOSEST-PAIR-DC.CC 
// Implementation of the divide-and-conquer
// closest-pair algorithm

#include <iostream>
#include <limits>

#include "ClosestPairDC.h"
#include "ClosestPairNaive.h"

using namespace std;

// bigger than any possible interpoint distance
const double INFTY = numeric_limits<double>::max(); 
//Point *cl1=new Point();
//Point *cl2=new Point();
//double cld=INFTY;
//
// findClosestPair()
//
// Given a collection of nPoints points, find and ***print***
//  * the closest pair of points
//  * the distance between them
// in the form "(x1, y1) (x2, y2) distance"
//

// INPUTS:
//  - points sorted in nondecreasing order by X coordinate
//  - points sorted in nondecreasing order by Y coordinate
//  - # of input points
//
struct res{
	double cld;
	Point *cl1;
	Point *cl2;
};
res findcl(Point *pointsByX[],Point *pointsByY[], int n/*int l,int r*/){

	//initial the result structure
	res re={INFTY,new Point(),new Point()};

	//base case
	if (n==1){
		return re;
	}
	if (n==2){
   	    re.cld=pointsByX[0]->distance(pointsByX[1]);
  		re.cl1=pointsByX[0];
   	    re.cl2=pointsByX[1];
		return re;
	}

	//divide and conquer
   	if (n>2) {
   		//split array
   		int mid = n/2;
   		Point *pbyl[mid];
   		Point *pbyr[n-mid];
   		Point *pbxl[mid];
   		Point *pbxr[n-mid];
   		int pl=0;
   		int pr=0;
   		for (int i=0;i<n;i++){
			if (pointsByY[i]->isLeftOf(pointsByX[mid])) {
   				pbyl[pl]=pointsByY[i];
   				pl++;
   			}else{
   				pbyr[pr]=pointsByY[i];
   				pr++;
			}
   		}
   		pl=0;
   		pr=0;
   		for (int i=0;i<mid;i++){
   				pbxl[i]=pointsByX[i];
   		}
   		for (int i=mid;i<n;i++){
   		   		pbxr[i-mid]=pointsByX[i];
   		}

   		//recursion
   		res re1=findcl(pbxl,pbyl,mid);
   		res re2=findcl(pbxr,pbyr,n-mid);
		
   		//find the closest distance
   		if (re1.cld<re.cld) {
   			re=re1;
   		}

   		if (re2.cld<re.cld) {
   		   	re=re2;
   		}
		
   		//find the x-axe range
   		int line1=mid-1;
   		int line2=mid+1;
   		while ((line1>=0)&&((pointsByX[mid-1]->x()-pointsByX[line1]->x())<re.cld)){
   			line1--;
   		}
   		while ((line2<n)&&((pointsByX[line2]->x()-pointsByX[mid-1]->x())<re.cld)){
   		   	line2++;
   		}
   		if (line1<0){
   			line1=0;
   		}
   		if(line2>=n){
   			line2=n-1;
   		}

   		//get the yStrip
   		pr=0;
   		int lll=line2-line1+1;
   		Point *pby[lll];
   		for (int i=0;i<n;i++){
   			if ( !(pointsByY[i]->isLeftOf(pointsByX[line2]))
   					||
					((pointsByY[i]->isLeftOf(pointsByX[line1])))  ){

   			}else{
   				pby[pr]=pointsByY[i];
   				pr++;
   			}
   		}
   		pby[lll-1]=pointsByX[line2];

   		//search the yStrip
   		int k=0;
   		for (int i=0;i<lll;i++){
   			k=1;
   			while(i+k<lll){
   			if((pby[i+k]->y()-pby[i]->y())>=re.cld){
   				break;
   			}
   				if (pby[i]->distance(pby[i+k])<re.cld){
   					re.cl1=pby[i];
   					re.cl2=pby[i+k];
   					re.cld=pby[i]->distance(pby[i+k]);
   				}
   				k++;
   			}
   		}
   		return re;
   	}
   	return re;
}

void findClosestPair(Point *pointsByX[], Point *pointsByY[],
		     int nPoints)
{
  //
  // Replace the following with your divide-and-conquer code.
  //
	const double INFTY = numeric_limits<double>::max();

	//initial
	res re={INFTY,new Point(),new Point()};

	//extreme situation protection
	Point *nn=new Point();
	if (nPoints==0){
		cout<<nn<<' '<<nn<<' '<<0<<endl;
		return;
	}

	//get result
	re=findcl(pointsByX,pointsByY,nPoints);
	//format and output
	if((re.cl1->isLeftOf(re.cl2))){
		cout<<re.cl1<<' '<<re.cl2<<' '<<re.cld<<endl;
	}else{
		cout<<re.cl2<<' '<<re.cl1<<' '<<re.cld<<endl;
	}
}

