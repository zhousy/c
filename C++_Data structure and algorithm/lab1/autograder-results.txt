*** RUNNING PRE-TURNIN TESTS ON 2015-02-20 21:22:29 CST ***
* Language is C++
* Compiled sources successfully

* Running test #1
* Arguments: @57
** DC:    (2411,29) (2419,53) 25.2982
** NAIVE: (2411,29) (2419,53) 25.2982
* Validating divide-and-conquer output
** OK
* Validating naive output
** OK
+++ PASSED test #1

* Running test #2
* Arguments: @421
** DC:    (786,78) (795,76) 9.21954
** NAIVE: (786,78) (795,76) 9.21954
* Validating divide-and-conquer output
** OK
* Validating naive output
** OK
+++ PASSED test #2

* Running test #3
* Arguments: @3216
** DC:    (105484,261) (105491,256) 8.60233
** NAIVE: (105484,261) (105491,256) 8.60233
* Validating divide-and-conquer output
** OK
* Validating naive output
** OK
+++ PASSED test #3

* Running test #4
* Arguments: @8192
** DC:    (-23573,2156) (-23572,2153) 3.16228
** NAIVE: (-23573,2156) (-23572,2153) 3.16228
* Validating divide-and-conquer output
** OK
* Validating naive output
** OK
+++ PASSED test #4

* Running test #5
* Arguments: @17436
** DC:    (7317,730) (7320,731) 3.16228
** NAIVE: (7317,730) (7320,731) 3.16228
* Validating divide-and-conquer output
** OK
* Validating naive output
** OK
+++ PASSED test #5

* Running test #6
* Arguments: @21213
** DC:    (-32718,4684) (-32718,4684) 0
** NAIVE: (-32718,4684) (-32718,4684) 0
* Validating divide-and-conquer output
** OK
* Validating naive output
** OK
+++ PASSED test #6

* Running test #7
* Arguments: lab1/zero-case.txt
** DC:    (12,21) (12,21) 0
** NAIVE: (12,21) (12,21) 0
* Validating divide-and-conquer output
** OK
* Validating naive output
** OK
+++ PASSED test #7

* Running test #8
* Arguments: lab1/y-case-1.txt
** DC:    (10,8) (10,7) 1
** NAIVE: (10,8) (10,7) 1
* Validating divide-and-conquer output
** OK
* Validating naive output
** OK
+++ PASSED test #8

* Running test #9
* Arguments: lab1/y-case-2.txt
** DC:    (17,8) (17,7) 1
** NAIVE: (17,8) (17,7) 1
* Validating divide-and-conquer output
** OK
* Validating naive output
** OK
+++ PASSED test #9

* Running test #10
* Arguments: lab1/stress.txt
** DC:    (-3,0) (3,0) 6
** NAIVE: (-3,93920) (3,93920) 6
* Validating divide-and-conquer output
** OK
* Validating naive output
** OK
+++ PASSED test #10

+++ SUMMARY: passed 10/10 tests
