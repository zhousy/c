//
// LAB1.CC
// Main driver code for CSE 241 Lab #1.
//
// WARNING: this file will be replaced with the unmodified
// original by the auto-grader and at turn-in time!

#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>
#include <limits>

#include <cstdlib>
#include <cmath>
#include <ctime>

#include "ClosestPairDC.h"
#include "ClosestPairNaive.h"

#include "Point.h"
#include "Timer.h"
#include "Prng.h"

using namespace std;

// Random seed, used when generating points at random.
static int seed = 9371;

///////////////////////////////////////////////////////////////////////
// Comparator functions for sort()

// compares p1 and p2 based on the x-coord
struct _lessThanX {
  bool operator() (const Point *p1, const Point *p2)
  { 
    return (p1->isLeftOf(p2));
  }
} lessThanX;

// compares p1 and p2 based on the y-coord
struct _lessThanY {
  bool operator()(const Point *p1, const Point *p2)
  { 
    return (p1->isBelow(p2));
  }
} lessThanY;


//
// genPointsAtRandom()
// Generate nPoints points uniformly at random.
//
static void genPointsAtRandom(Point *points[], int nPoints,
			      PRNG *prng)
{
  double x = 0.0;
  double y = 0.0;

  double step = sqrt(nPoints);
  
  for (int j = 0; j < nPoints; j++)
    {
      // bump next point's X coordinate
      x += 10000.0 * (prng->nextDouble() - 0.5);
      
      // move the Y coordinate a random amount up,
      // while keeping it within limits [0 .. nPoints)
      y = (y + step * prng->nextDouble());
      while (y > nPoints)
	y -= nPoints;
	   
      Point *p = new Point;
      p->setX((int) rint(x)); // set x and y coords of point
      p->setY((int) rint(y));
      points[j] = p;
    }
}


//
// readXYPoints()
// Read a collection of points from an input file.
// The first line of the file should give the number of points
// to read.  Each subsequent line should give the coordinates of
// one point in the form "x y".
//
// RETURNS: array of pointers to Point structures
// SETS: *inPoints to length of array
//
static Point **readXYPoints(const char *pointFileName, int *inPoints)
{
  ifstream pointFile(pointFileName);
  int nPoints;
  
  if (!pointFile)
    {
      cerr << "Error: could not open point file '"
	   << pointFileName << "'" << endl;
      exit(1);
    }
  
  pointFile >> nPoints;
  
  if (nPoints < 2)
    {
      cerr << "Error: need at least two points!" << endl;
      exit(1);
    }
  
  Point **points = new Point * [nPoints];
  
  for (int j = 0; j < nPoints; j++)
    {
      int x, y;
      
      pointFile >> x;
      pointFile >> y;
      
      if (!pointFile)
	{
	  cerr << "Error: could not read point #" << j << endl;
	  exit(1);
	}
      
      Point *p = new Point(x,y);
      
      points[j] = p;
    }
  
  *inPoints = nPoints;
  
  return points;
}


int main(int argc, char *argv[])
{
  Point **points;      // array of refs to points
  int nPoints;         // number of points
  Timer timer;  
  string fileName;
  
  // only argument is the input spec
  if (argc > 1)
    {
      fileName = argv[1];
    }
  else
    {
      cerr << "Syntax: Lab1 <filename>\n";
      exit(1);
    }
  
  // A filename argument of the form '@x', where x is a non-negative
  // integer, allocates x random points.  Any other argument is
  // assumed to be a file from which points are read.
  //int a[]={10000,20000,40000,60000,80000,100000};
  //for (int j=10;j<=150;j=j+10){
	  //cout<<"-------------------------------------"<<endl;
	  //cout<<(j)<<" ";
	  //cout<<"-------------------------------------"<<endl;
	 // double av_n=0;
	  //double av_d=0;
  //for (int i=0;i<20000;i++){
  if (fileName[0] != '@')
    {
      points = readXYPoints(argv[1], &nPoints);
    }
  else
    {
      nPoints = strtoul(fileName.substr(1).c_str(), NULL, 10);
  //nPoints=j;
      // Use these lines if you want a different random set of points for each
      // run.  This code sets the seed for the RNG from the system clock time.
      //
      // {
      //  time_t tp;
      //  time(&tp);
      //  seed = tp;
      // }
      
      PRNG prng(seed);  // seed the random number generator
      
      points = new Point * [nPoints];
      genPointsAtRandom(points, nPoints, &prng);
    }
  
  if (nPoints < 2)
    {
      cerr << "ERROR: input must contain at least two points\n";
      exit(1);
    }
  
  timer.start();
  
  //////////////////////////////////////////////////////////////////////////
  // CLOSEST-PAIR ALGORITHM STARTS HERE
  
  // The algorithm expects two arrays containing the same points.
  Point **pointsByX = new Point * [nPoints];
  Point **pointsByY = new Point * [nPoints];
  
  for (int j = 0; j < nPoints; j++)
    {
      pointsByX[j] = points[j];
      pointsByY[j] = points[j];
    }
  
  // NB: you should *not* have to call sort() in your
  // own code!
  sort(pointsByX, pointsByX + nPoints, lessThanX); // sort by x-coord to get X
  sort(pointsByY, pointsByY + nPoints, lessThanY); // sort by y-coord to get Y
  
  findClosestPair(pointsByX, pointsByY, nPoints);
  
  // CLOSEST-PAIR ALGORITHM ENDS HERE
  //////////////////////////////////////////////////////////////////////////
  
  timer.stop();
  
  cout << "For n = " << nPoints << ", the time is ";
  cout << timer.elapsedTime() << " milliseconds\n\n";
  //av_d+=timer.elapsedTime();
  //cout<<timer.elapsedTime()<<"; ";
  timer.start();

  ///////////////////////////////////////////////////////////////////////
  // NAIVE CLOSEST-PAIR ALGORITHM STARTS HERE
  
  findClosestPairNaive(points, nPoints);
  
  // NAIVE CLOSEST-PAIR ALGORITHM ENDS HERE
  ///////////////////////////////////////////////////////////////////////
  
  timer.stop();
  
  cout << "For n = " << nPoints << ", the naive time is ";
  cout << timer.elapsedTime() << " milliseconds\n\n";
  //cout<<timer.elapsedTime()<<"; "<<endl;
  //av_n+=timer.elapsedTime();
  

  delete [] pointsByX;   // free storage from array ptsByX of pt refs 
  delete [] pointsByY;   // free storage from array ptsByY of pt refs
  
  for (int j = 0; j < nPoints; j++)
    delete points[j];  // free storage from points themselves
  
  delete [] points;

  //cout<<(av_d/1000.0)<<" "<<(av_n/1000.0)<<endl;
  //}
  return 0;
}
